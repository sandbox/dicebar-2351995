<?php

/**
 * Page callback for /admin/config/content/node-changed-filter.
 */
function node_changed_filter_overview() {
  // No results.
  $types = node_changed_filter_types();
  if (empty($types)) {
    return array(
      '#markup' => t('No node types are managed yet by Node Changed Filter.'),
    );
  }

  // Build table.
  $table = array(
    '#theme' => 'table',
  );

  // Build header for table.
  $header = array();
  $header[] = t('Node type');
  $header[] = t('Actions');

  // Add header to table.
  $table['#header'] = $header;

  // Build rows.
  $rows = array();
  $node_types = node_type_get_names();
  foreach ($types as $type) {
    // Ensure the node type still exists.
    if (isset($node_types[$type])) {
      // Actions to be performed on the node type.
      $actions = array();
      $actions[] = l(t('settings'), 'admin/config/content/node-changed-filter/settings/' . str_replace('_', '-', $type));

      // Build row.
      $rows[] = array(
        $node_types[$type],
        implode(' ', $actions),
      );
    }
  }

  // Add rows to table.
  $table['#rows'] = $rows;

  // Return table.
  return $table;
}

/**
 * Form callback for /admin/config/content/node-changed-filter/types.
 */
function node_changed_filter_types_form($form, &$form_state) {
  // Update page title.
  $item = menu_get_item();
  drupal_set_title($item['title']);

  // Load configured node types.
  $node_types = node_type_get_names();
  $types = node_changed_filter_types();

  // Build default values.
  $default_value = array();
  foreach ($node_types as $node_type => $label) {
    // Set default value based on the node_changed_filter_types().
    if (in_array($node_type, $types)) {
      $default_value[$node_type] = $node_type;  
    }
    else {
      $default_value[$node_type] = 0;
    }
  }

  // List of node types.
  $form['types'] = array(
    '#title' => 'Node types',
    '#type' => 'checkboxes',
    '#options' => $node_types,
    '#default_value' => $default_value,
  );

  // Actions.
  $form['actions'] = array(
    '#type' => 'actions',
  );

  // Submit.
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );

  // Cancel.
  $form['actions']['cancel'] = array(
    '#theme' => 'link',
    '#path' => 'admin/config/content/node-changed-filter',
    '#text' => t('Cancel'),
    // Attributes and HTML are required...bad Drupal! Bad!
    '#options' => array(
      'attributes' => array(
      ),
      'html' => FALSE,
    ),
  );

  return $form;
}

/**
 * Submit callback for node_changed_filter_types_form().
 */
function node_changed_filter_types_form_submit($form, &$form_state) {
  $types = array();
  if (isset($form_state['values']['types'])) {
    foreach ($form_state['values']['types'] as $type) {
      if ($type) {
        $types[] = $type;
      }
    }
  }

  // Store types.
  node_changed_filter_types($types);

  // Inform user of success.
  drupal_set_message(t('The node types have been updated.'));

  // Redirect back to overview.
  $form_state['redirect'] = 'admin/config/content/node-changed-filter';
}

/**
 * Access callback for /admin/config/content/node-changed-filter/settings/%.
 */
function node_changed_filter_type_settings_form($form, &$form_state, $type) {
  // Convert node type to machine name.
  $type = str_replace('-', '_', $type);
  $form_state['node_type'] = $type;
  $settings = node_changed_filter_type_settings($type);

  // Set title.
  $node_types = node_type_get_names();
  if (isset($node_types[$type])) {
    drupal_set_title(t('@node_type settings', array(
      '@node_type' => $node_types[$type],
    )));
  }

  // Get property info.
  $info = entity_get_property_info('node');

  // Properties fieldset.
  $form['properties'] = array(
    '#collapsed' => TRUE,
    '#collapsible' => TRUE,
    '#title' => t('Properties'),
    '#type' => 'fieldset',
  );

  // Don't show blacklisted properties.
  $blacklist = _node_changed_filter_property_blacklist();
  foreach ($info['properties'] as $key => $property) {
    if (!in_array($key, $blacklist)) {
      // Enabled status.
      $enabled = ((isset($settings['properties'])) && (in_array($key, $settings['properties'])));

      // Checkbox.
      $form['properties'][$key] = array(
        '#type' => 'checkbox',
        '#title' => $property['label'],
        '#description' => (isset($property['description'])) ? $property['description'] : NULL,
        '#parents' => array('properties', $key),
        '#default_value' => $enabled,
      );
    }
  }

  // Only show fields fieldset if fields have been defined.
  if (!empty($info['bundles'][$type]['properties'])) {
    // Fields fieldset.
    $form['fields'] = array(
      '#collapsed' => TRUE,
      '#collapsible' => TRUE,
      '#title' => t('Fields'),
      '#type' => 'fieldset',
    );

    foreach ($info['bundles'][$type]['properties'] as $key => $info) {
      // Enabled status.
      $enabled = ((isset($settings['fields'])) && (in_array($key, $settings['fields'])));

      // Checkbox.
      $form['fields'][$key] = array(
        '#type' => 'checkbox',
        '#title' => $info['label'],
        '#description' => (isset($info['description'])) ? $info['description'] : NULL,
        '#parents' => array('fields', $key),
        '#default_value' => $enabled,
      );
    }
  }

  // Actions.
  $form['actions'] = array(
    '#type' => 'actions',
  );

  // Submit.
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );

  // Cancel.
  $form['actions']['cancel'] = array(
    '#theme' => 'link',
    '#path' => 'admin/config/content/node-changed-filter',
    '#text' => t('Cancel'),
    // Attributes and HTML are required...bad Drupal! Bad!
    '#options' => array(
      'attributes' => array(
      ),
      'html' => FALSE,
    ),
  );

  return $form;
}

/**
 * Submit handler for node_changed_filter_type_settings_form().
 */
function node_changed_filter_type_settings_form_submit($form, &$form_state) {
  $type = $form_state['node_type'];

  // Empty settings array.
  $settings = node_changed_filter_type_settings_defaults();

  // Create settings for properties.
  if (isset($form_state['values']['properties'])) {
    foreach ($form_state['values']['properties'] as $key => $value) {
      if ($value) {
        $settings['properties'][] = $key;
      }
    }
  }

  // Create settings for fields.
  if (isset($form_state['values']['fields'])) {
    foreach ($form_state['values']['fields'] as $key => $value) {
      if ($value) {
        $settings['fields'][] = $key;
      }
    }
  }

  // Store values.
  node_changed_filter_type_settings($type, $settings);

  // Inform user of success.
  $node_types = node_type_get_names();
  drupal_set_message(t('The settings for the @node_type content type have been updated.', array(
    '@node_type' => $node_types[$type],
  )));

  // Redirect back to overview.
  $form_state['redirect'] = 'admin/config/content/node-changed-filter';
}

/**
 * Helper function for node_changed_filter_type_settings_form().
 *
 * Returns an array of black-listed properties.
 */
function _node_changed_filter_property_blacklist() {
  return array(
    'changed',
    'is_new',
    'nid',
    'uuid',
    'body',
  );
}

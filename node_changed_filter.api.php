<?php

/**
 * This hook allows modules to alter propery data before it is compared, where
 * @param $data is the data stored in the property defined in @param $property.
 */
function hook_node_changed_filter_properties($property, $data, $context) {
  switch ($property) {
    case 'log':
      // Return false if the previous version of the node is being processed.
      if ($context['current'] == 'original') {
        return FALSE;
      }

      // Return a boolean based on whether or not a log message has been entered.
      $data = (!empty($data));
      break;
  }

  return $data;
}

/**
 * This hook allows modules to alter field data before it is compared, where
 * @param $data is the data stored in the field defined in @param $instance.
 */
function hook_node_changed_filter_fields($instance, $data, $context) {
  $field = field_info_field($instance['field_name']);

  switch ($field['type']) {
    case 'text_with_summary':
      $rebuild = array();
      foreach ($data as $language => $item) {
        foreach ($item as $delta => $columns) {
          $rebuild[$language][$delta] = array(
            'summary' => _text_sanitize($instance, $language, $columns, 'summary'),
            'value' => _text_sanitize($instance, $language, $columns, 'value'),
          );
        }
      }
      $data = $rebuild;
      break;
  }

  return $data;
}
